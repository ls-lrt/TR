#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux-x86
CND_CONF=Release
CND_DISTDIR=dist

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=build/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/lib_destijl/source/dserver.o \
	${OBJECTDIR}/fonctions.o \
	${OBJECTDIR}/lib_destijl/source/dtools.o \
	${OBJECTDIR}/lib_destijl/source/drobot.o \
	${OBJECTDIR}/lib_destijl/source/djpegimage.o \
	${OBJECTDIR}/lib_destijl/source/dmouvement.o \
	${OBJECTDIR}/lib_destijl/source/darena.o \
	${OBJECTDIR}/global.o \
	${OBJECTDIR}/lib_destijl/source/dimage.o \
	${OBJECTDIR}/main.o \
	${OBJECTDIR}/lib_destijl/source/dmessage.o \
	${OBJECTDIR}/lib_destijl/source/dimageshop.o \
	${OBJECTDIR}/lib_destijl/source/dposition.o \
	${OBJECTDIR}/lib_destijl/source/dbattery.o \
	${OBJECTDIR}/lib_destijl/source/dmission.o \
	${OBJECTDIR}/lib_destijl/source/daction.o \
	${OBJECTDIR}/lib_destijl/source/dcamera.o

# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	${MAKE}  -f nbproject/Makefile-Release.mk dist/Release/GNU-Linux-x86/projet_destijl

dist/Release/GNU-Linux-x86/projet_destijl: ${OBJECTFILES}
	${MKDIR} -p dist/Release/GNU-Linux-x86
	${LINK.c} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/projet_destijl ${OBJECTFILES} ${LDLIBSOPTIONS} 

${OBJECTDIR}/lib_destijl/source/dserver.o: nbproject/Makefile-${CND_CONF}.mk lib_destijl/source/dserver.c 
	${MKDIR} -p ${OBJECTDIR}/lib_destijl/source
	${RM} $@.d
	$(COMPILE.c) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/lib_destijl/source/dserver.o lib_destijl/source/dserver.c

${OBJECTDIR}/fonctions.o: nbproject/Makefile-${CND_CONF}.mk fonctions.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.c) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/fonctions.o fonctions.c

${OBJECTDIR}/lib_destijl/source/dtools.o: nbproject/Makefile-${CND_CONF}.mk lib_destijl/source/dtools.c 
	${MKDIR} -p ${OBJECTDIR}/lib_destijl/source
	${RM} $@.d
	$(COMPILE.c) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/lib_destijl/source/dtools.o lib_destijl/source/dtools.c

${OBJECTDIR}/lib_destijl/source/drobot.o: nbproject/Makefile-${CND_CONF}.mk lib_destijl/source/drobot.c 
	${MKDIR} -p ${OBJECTDIR}/lib_destijl/source
	${RM} $@.d
	$(COMPILE.c) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/lib_destijl/source/drobot.o lib_destijl/source/drobot.c

${OBJECTDIR}/lib_destijl/source/djpegimage.o: nbproject/Makefile-${CND_CONF}.mk lib_destijl/source/djpegimage.c 
	${MKDIR} -p ${OBJECTDIR}/lib_destijl/source
	${RM} $@.d
	$(COMPILE.c) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/lib_destijl/source/djpegimage.o lib_destijl/source/djpegimage.c

${OBJECTDIR}/lib_destijl/source/dmouvement.o: nbproject/Makefile-${CND_CONF}.mk lib_destijl/source/dmouvement.c 
	${MKDIR} -p ${OBJECTDIR}/lib_destijl/source
	${RM} $@.d
	$(COMPILE.c) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/lib_destijl/source/dmouvement.o lib_destijl/source/dmouvement.c

${OBJECTDIR}/lib_destijl/source/darena.o: nbproject/Makefile-${CND_CONF}.mk lib_destijl/source/darena.c 
	${MKDIR} -p ${OBJECTDIR}/lib_destijl/source
	${RM} $@.d
	$(COMPILE.c) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/lib_destijl/source/darena.o lib_destijl/source/darena.c

${OBJECTDIR}/global.o: nbproject/Makefile-${CND_CONF}.mk global.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.c) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/global.o global.c

${OBJECTDIR}/lib_destijl/source/dimage.o: nbproject/Makefile-${CND_CONF}.mk lib_destijl/source/dimage.c 
	${MKDIR} -p ${OBJECTDIR}/lib_destijl/source
	${RM} $@.d
	$(COMPILE.c) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/lib_destijl/source/dimage.o lib_destijl/source/dimage.c

${OBJECTDIR}/main.o: nbproject/Makefile-${CND_CONF}.mk main.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.c) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/main.o main.c

${OBJECTDIR}/lib_destijl/source/dmessage.o: nbproject/Makefile-${CND_CONF}.mk lib_destijl/source/dmessage.c 
	${MKDIR} -p ${OBJECTDIR}/lib_destijl/source
	${RM} $@.d
	$(COMPILE.c) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/lib_destijl/source/dmessage.o lib_destijl/source/dmessage.c

${OBJECTDIR}/lib_destijl/source/dimageshop.o: nbproject/Makefile-${CND_CONF}.mk lib_destijl/source/dimageshop.c 
	${MKDIR} -p ${OBJECTDIR}/lib_destijl/source
	${RM} $@.d
	$(COMPILE.c) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/lib_destijl/source/dimageshop.o lib_destijl/source/dimageshop.c

${OBJECTDIR}/lib_destijl/source/dposition.o: nbproject/Makefile-${CND_CONF}.mk lib_destijl/source/dposition.c 
	${MKDIR} -p ${OBJECTDIR}/lib_destijl/source
	${RM} $@.d
	$(COMPILE.c) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/lib_destijl/source/dposition.o lib_destijl/source/dposition.c

${OBJECTDIR}/lib_destijl/source/dbattery.o: nbproject/Makefile-${CND_CONF}.mk lib_destijl/source/dbattery.c 
	${MKDIR} -p ${OBJECTDIR}/lib_destijl/source
	${RM} $@.d
	$(COMPILE.c) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/lib_destijl/source/dbattery.o lib_destijl/source/dbattery.c

${OBJECTDIR}/lib_destijl/source/dmission.o: nbproject/Makefile-${CND_CONF}.mk lib_destijl/source/dmission.c 
	${MKDIR} -p ${OBJECTDIR}/lib_destijl/source
	${RM} $@.d
	$(COMPILE.c) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/lib_destijl/source/dmission.o lib_destijl/source/dmission.c

${OBJECTDIR}/lib_destijl/source/daction.o: nbproject/Makefile-${CND_CONF}.mk lib_destijl/source/daction.c 
	${MKDIR} -p ${OBJECTDIR}/lib_destijl/source
	${RM} $@.d
	$(COMPILE.c) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/lib_destijl/source/daction.o lib_destijl/source/daction.c

${OBJECTDIR}/lib_destijl/source/dcamera.o: nbproject/Makefile-${CND_CONF}.mk lib_destijl/source/dcamera.c 
	${MKDIR} -p ${OBJECTDIR}/lib_destijl/source
	${RM} $@.d
	$(COMPILE.c) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/lib_destijl/source/dcamera.o lib_destijl/source/dcamera.c

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf:
	${RM} -r build/Release
	${RM} dist/Release/GNU-Linux-x86/projet_destijl

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
