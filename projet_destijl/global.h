/* 
 * File:   global.h
 * Author: pehladik
 *
 * Created on 12 janvier 2012, 10:11
 */

#ifndef GLOBAL_H
#define	GLOBAL_H

#include "includes.h"

#define MISSION_ACCEPTABLE_RANGE 50

/* @descripteurs des tâches */
extern RT_TASK tServeur;
extern RT_TASK tconnect;
extern RT_TASK tmove;
extern RT_TASK twatchdog;
extern RT_TASK tbattery;
extern RT_TASK tstreaming;
extern RT_TASK tmission;

/* @descripteurs des mutex */
extern RT_MUTEX mutexEtat;
extern RT_MUTEX mutexMove;
extern RT_MUTEX mutexEtatCamera; /* mutex pour accéder au booléen "etat_camera" */
extern RT_MUTEX mutexPosition;

/* @descripteurs des sempahore */
extern RT_SEM semConnecterRobot;
extern RT_SEM semDeplacer;
extern RT_SEM semRechargerWatchdog;
extern RT_SEM semVerifierBatterie;
extern RT_SEM semMissionLancee;

/* @Variables partagées */
extern int etatCommMoniteur;
extern int etatCommRobot;
extern int cptCommErr;
extern int etat_camera;
extern DServer *serveur;
extern DRobot *robot;
extern DMovement *move;
extern DCamera *camera;

/* @État du robot et de l'arène */
extern DArena *arena;
extern DPosition *position_robot;
extern DPosition *position_dest;
extern DMission *mission_s;
extern float x_arena, y_arena, x_robot, y_robot, orientation_robot;

#endif	/* GLOBAL_H */

