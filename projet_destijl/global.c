/*
 * File:   global.h
 * Author: pehladik
 *
 * Created on 21 avril 2011, 12:14
 */

#include "global.h"

RT_TASK tServeur;
RT_TASK tconnect;
RT_TASK tmove;
RT_TASK twatchdog;
RT_TASK tbattery;
RT_TASK tstreaming;
RT_TASK tmission;

RT_MUTEX mutexEtat;
RT_MUTEX mutexMove;
RT_MUTEX mutexEtatCamera;
RT_MUTEX mutexPosition;

RT_SEM semConnecterRobot;
RT_SEM semDeplacer;
RT_SEM semRechargerWatchdog;
RT_SEM semVerifierBatterie;
RT_SEM semMissionLancee;

int etatCommMoniteur = 1;
int etatCommRobot = 1;
int cptCommErr = 0; /* compteur global d'erreurs de communication */
int etat_camera = 0; /* état de la caméra: 0 => streaming de base; 1 => streaming avec arène; 2 => streaming avec position robot */
DRobot *robot;
DMovement *move;
DServer *serveur;
DCamera *camera;

DArena *arena;
DPosition *position_robot;
DPosition *position_dest;
DMission *mission_s;
float x_arena, y_arena, x_robot, y_robot, orientation_robot;
